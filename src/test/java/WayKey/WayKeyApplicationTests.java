package WayKey;

import WayKey.DAO.UserDAO;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
class WayKeyApplicationTests {
    @Test
    void userDAOtest() {
        DBconnector.ConnectDB();

        // add user to database
//        assertEquals(201,UserDAO.addUserToDB("wktest","userdao201","ud2","wkDefaulConfirmCode"));
//        assertEquals(412,UserDAO.addUserToDB("wktest","userdao412","ud2","a"));

        //register
//        assertEquals(202,UserDAO.registerUser("not exists","not_exists","not_exists","hozzchef@gmail.com"));
//        assertEquals(302,UserDAO.registerUser("exists","wktest","wktest","hozzchef@gmail.com"));

        // login
//        assertEquals(412,UserDAO.loginUser("wktest","wrong_password",null,null));
//        assertEquals(404,UserDAO.loginUser("wrong_account","wktest",null,null));
//        assertEquals(200,UserDAO.loginUser("wktest","wktest",null,null));

        // addFriend
//        assertEquals(403,UserDAO.addFriend(1));
//        assertEquals(403,UserDAO.addFriend(14));
//        assertEquals(403,UserDAO.addFriend(15));
//        assertEquals(201,UserDAO.addFriend(0));
//        assertEquals(201,UserDAO.addFriend(-1));

        // edit
//        assertEquals(202,UserDAO.edit("wk test account","status",null));

        // autoLogin
//        assertEquals(200,UserDAO.autoLogin("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36"));
//        assertEquals(404,UserDAO.autoLogin("user agent not found"));

        // logout
//        assertEquals(200,UserDAO.logout());
    }
}
