package WayKey;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Locale;

public class NotJunit {
    public static void main(String[] args) {
        long startTime;
        long endTime;


        startTime = System.nanoTime();
        for (int i = 0; i < 101; i++) {
            stringifyDate(new Date().toString());
            stringifyDate("Tue Sep 28 11:30:20 MSK 2022");
            stringifyDate("Tue Sep 28 10:30:20 MSK 2022");
            stringifyDate("Tue Sep 28 8:20:20 MSK 2022");
            stringifyDate("Mon Sep 26 14:00:20 MSK 2022");
            stringifyDate("Mon Sep 23 15:00:20 MSK 2022");
            stringifyDate("Mon Aug 27 15:00:20 MSK 2022");
            stringifyDate("Mon Jan 27 15:00:20 MSK 2022");
            stringifyDate("Tue Sep 27 15:00:20 MSK 2021");
            stringifyDate("Tue Sep 27 15:00:20 MSK 2019");
        }
        endTime = System.nanoTime();
        System.out.println("v1 -> " + ((endTime - startTime) / 1000000) + " ms");

        startTime = System.nanoTime();
        for (int i = 0; i < 101; i++) {
            stringifyDateV2(new Date().toString());
            stringifyDateV2("Tue Sep 28 11:30:20 MSK 2022");
            stringifyDateV2("Tue Sep 28 10:30:20 MSK 2022");
            stringifyDateV2("Tue Sep 28 8:20:20 MSK 2022");
            stringifyDateV2("Mon Sep 26 14:00:20 MSK 2022");
            stringifyDateV2("Mon Sep 23 15:00:20 MSK 2022");
            stringifyDateV2("Mon Aug 27 15:00:20 MSK 2022");
            stringifyDateV2("Mon Jan 27 15:00:20 MSK 2022");
            stringifyDateV2("Tue Sep 27 15:00:20 MSK 2021");
            stringifyDateV2("Tue Sep 27 15:00:20 MSK 2019");
        }
        endTime = System.nanoTime();
        System.out.println("v2 -> " + ((endTime - startTime) / 1000000) + " ms\n");

        System.out.println(stringifyDateV2(new Date().toString())); // только что
        System.out.println(stringifyDateV2("Tue Sep 28 22:40:20 MSK 2022")); // X минут назад (смотря когда доделаю)
        System.out.println(stringifyDateV2("Tue Sep 28 10:30:20 MSK 2022")); // час назад
        System.out.println(stringifyDateV2("Tue Sep 28 8:20:20 MSK 2022")); // 3 часа назад
        System.out.println(stringifyDateV2("Mon Sep 26 14:00:20 MSK 2022")); // вчера
        System.out.println(stringifyDateV2("Mon Sep 23 15:00:20 MSK 2022")); // 4 дня назад
        System.out.println(stringifyDateV2("Mon Aug 27 15:00:20 MSK 2022")); // месяц назад
        System.out.println(stringifyDateV2("Mon Jan 27 15:00:20 MSK 2022")); // 8 месяцев назад
        System.out.println(stringifyDateV2("Tue Sep 27 15:00:20 MSK 2021")); // год назад
        System.out.println(stringifyDateV2("Tue Sep 27 15:00:20 MSK 2019")); // 3 года назад

    }
    private static int getMonth(String month) {
        switch (month.toLowerCase()) {
            case "jan" -> {
                return 1;
            }
            case "feb" -> {
                return 2;
            }
            case "mar" -> {
                return 3;
            }
            case "apr" -> {
                return 4;
            }
            case "may" -> {
                return 5;
            }
            case "jun" -> {
                return 6;
            }
            case "jul" -> {
                return 7;
            }
            case "aug" -> {
                return 8;
            }
            case "sep" -> {
                return 9;
            }
            case "oct" -> {
                return 10;
            }
            case "nov" -> {
                return 11;
            }
            case "dec" -> {
                return 12;
            }
        }
        return 0;
    }
    public static String stringifyDate(String dateString) {
        /*
        X минут назад = прошло меньше часа
        X часов назад = прошло меньше суток
        вчера = от 1 до 2 суток
        X дней назад = от 2 до 7 суток
        месяц назад = от 1 до 2 месяцев
        X месяцев назад = от 2 до 12 месяцев
        год назад = от 1 до 2 лет
        X лет назад = от 2 лет
         */

        // раскладываем дату на составные
        String[] date = dateString.split(" ");
        int month = getMonth(date[1]);
        int day = Integer.parseInt(date[2]);
        String time = date[3];
        int year = Integer.parseInt(date[5]);

        // раскладываем текущую дату на составные
        String[] currentDate = new Date().toString().split(" ");
        int currentMonth = getMonth(currentDate[1]);
        int currentDay = Integer.parseInt(currentDate[2]);
        String currentTime = currentDate[3];
        int currentYear = Integer.parseInt(currentDate[5]);

        // смотрим сколько времени прошло от текущей даты
        int yearsPassed = currentYear - year;
        int monthsPassed = currentMonth - month + (12 * yearsPassed);
        int daysPassed = currentDay - day;
        int hoursPassed = Integer.parseInt(currentTime.split(":")[0]) - Integer.parseInt(time.split(":")[0]);
        int minutesPassed = Integer.parseInt(currentTime.split(":")[1]) - Integer.parseInt(time.split(":")[1]);

//        System.out.println("yearsPassed -> " + yearsPassed);
//        System.out.println("monthsPassed -> " + monthsPassed);
//        System.out.println("daysPassed -> " + daysPassed);
//        System.out.println("hoursPassed -> " + hoursPassed);
//        System.out.println("minutesPassed -> " + minutesPassed);

        // минуты
        if (
                yearsPassed == 0 &&
                monthsPassed == 0 &&
                daysPassed == 0 &&
                hoursPassed == 0
        ) return minutesPassed != 0 ? minutesPassed + " минут назад" : "только что";
        // часы
        else if (
                yearsPassed == 0 &&
                monthsPassed == 0 &&
                daysPassed == 0
        ) return hoursPassed != 1 ? hoursPassed + " часов назад" : "час назад";
        // дни
        else if (
                yearsPassed == 0 &&
                monthsPassed == 0
        ) return daysPassed != 1 ? daysPassed + " дней назад" : "вчера";
        // месяцы
        else if (
                yearsPassed == 0
        ) return monthsPassed != 1 ? monthsPassed + " месяцев назад" : "месяц назад";
        // годы
        else return yearsPassed != 1 ? yearsPassed + " лет назад" : "год назад";
    }
    public static String stringifyDateV2(String dateString) {
        // разделяем даты на составные
        String[] date = dateString.split(" ");
        String[] currentDate = new Date().toString().split(" ");

        // создаем константы для облегчения вычисления и читабельности
        int minutesInYear = 525960;
        int minutesInMonth = 43800;
        int minutesInDay = 1440;
        int minutesInHour = 60;

        long minutesPassed = 0
                + Integer.parseInt(currentDate[3].split(":")[1]) - Integer.parseInt(date[3].split(":")[1]) // минуты
                + (minutesInHour * (Integer.parseInt(currentDate[3].split(":")[0]) - Integer.parseInt(date[3].split(":")[0]))) // часы
                + (minutesInDay * (Integer.parseInt(currentDate[2]) - Integer.parseInt(date[2]))) // дни
                + (minutesInMonth * (getMonth(currentDate[1]) - getMonth(date[1]))) // месяцы
                + (minutesInYear * (Integer.parseInt(currentDate[5]) - Integer.parseInt(date[5]))); // года

        // выдаем результат в зависимости от случая
        if (minutesPassed < minutesInHour) return minutesPassed != 0 ? minutesPassed + " минут назад" : "только что";// минуты
        else if (minutesPassed < minutesInDay) return minutesPassed > (minutesInHour * 2) ? (minutesPassed / minutesInHour) + " часов назад" : "час назад";// часы
        else if (minutesPassed < minutesInMonth) return minutesPassed > (minutesInDay * 2) ? (minutesPassed / minutesInDay) + " дней назад" : "вчера";// дни
        else if (minutesPassed < minutesInYear) return minutesPassed > (minutesInMonth * 2) ? (minutesPassed / minutesInMonth) + " месяцев назад назад" : "месяц назад";// месяцы
        else return minutesPassed > (minutesInYear * 2) ? (minutesPassed / minutesInYear) + " лет назад" : "год назад";// годы
    }
}
