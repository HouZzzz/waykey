package WayKey;

import WayKey.DAO.UserDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WayKeyApplication {
	private static Logger LOG = LoggerFactory.getLogger(UserDAO.class);
	public static void main(String[] args) {
		System.out.println("LOG INFO\n");
		System.out.println("info enabled " + LOG.isInfoEnabled());
		System.out.println("debug enabled " + LOG.isDebugEnabled());
		System.out.println("\n");
		if (DBconnector.ConnectDB()) {
			LOG.info("Соеденение с базой данных создано успешно");
			SpringApplication.run(WayKeyApplication.class, args);

			// периодическое обновление соединения с базой данных
			new Thread(() -> {
				while (true) {
					try {
						Thread.sleep(1000 * 60  * 3); // раз в 3 мин
					} catch (InterruptedException e) {
						throw new RuntimeException(e);
					}
					DBconnector.ConnectDB();

				}
			}).start();
		} else {
			LOG.info("Ошибка при соеденении с базой данных");
		}
	}

}
