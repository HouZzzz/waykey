package WayKey.Logic;

import java.util.Random;

public class MessageEncoder {

    public static String encodeMessage(String text) {
        Random random = new Random();
        String encodedMessage = "";

        for (char c : text.toCharArray()) {
            int shift = 9 - random.nextInt(8);
            char symbol = (char) (((int) c) + shift);
            if (symbol == '\'') {
                shift++;
                symbol = (char) (((int) c) + shift);
            }
            encodedMessage += symbol + (shift + " ");
        }
        return encodedMessage.trim();
    }

    public static String decodeMessage(String text) {
        String decodedMessage = "";

        String[] charsList = text.split(" ");
        for (String s : charsList) {
            String key = s.split("")[1];
            String value = s.split("")[0];

            decodedMessage += (char) (((int) value.charAt(0)) - Integer.parseInt(key));
        }
        return decodedMessage;
    }
}
