package WayKey.Logic;

import java.util.Random;

public class KeyCreator {
    public static String generate4dKey() {
        Random random = new Random();
        String key = Integer.toString(random.nextInt(9999));
        while (key.length() < 4) {
            key = Integer.toString((Integer.parseInt(key) * 10) + random.nextInt(9));
        }


        return key;
    }
}
