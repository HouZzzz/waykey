package WayKey;

import WayKey.DAO.UserDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;

public class DBconnector {
    private static final String URL = "jdbc:mysql://127.0.0.1:3306/waykeydb";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "root";

    private static Connection connection;
    private static Statement statement;
    private static Logger LOG = LoggerFactory.getLogger(DBconnector.class);

    public static boolean ConnectDB() {
        try {
            Driver driver = new com.mysql.cj.jdbc.Driver();
            DriverManager.registerDriver(driver);
        } catch (SQLException e) {
            return false;
        }

        try {
            connection = DriverManager.getConnection(URL,USERNAME,PASSWORD);
            statement = connection.createStatement();
        } catch (SQLException e) {
            return false;
        }
        LOG.info("Обновлено соединение с базой данных");
        return true;
    }

    public static Connection getConnection() {
        return DBconnector.connection;
    }

    public static Statement getStatement() {
        return DBconnector.statement;
    }


}
