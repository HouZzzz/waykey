package WayKey.Servlets;

import WayKey.DAO.PostDAO;
import WayKey.DAO.UserDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.io.IOException;

//@WebServlet("/edit")
@MultipartConfig(fileSizeThreshold=1024*1024*10, 	// 10 MB
        maxFileSize=1024*1024*50,      	// 50 MB
        maxRequestSize=1024*1024*100)   	// 100 MB
public class FileUploadServlet extends HttpServlet {
    public static void edit(HttpServletRequest request) throws ServletException, IOException {
        Part filePart = request.getPart("photo");
        UserDAO.edit(request.getParameter("name"),request.getParameter("status"),filePart,request.getHeader("User-Agent"));
    }
    public static void addPhotoToPost(HttpServletRequest request) throws ServletException, IOException {
        String text = request.getParameter("postText");
        Part photo = request.getPart("photo");
        PostDAO.createPost(text,photo,request.getHeader("User-Agent"));
    }
}
