package WayKey.Controllers;

import WayKey.DAO.MessageDAO;
import WayKey.DataContainer;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@Controller
public class MessangerController {
    @GetMapping("/messanger")
    public static String messanger(Model model,@RequestHeader(value = "User-Agent") String userAgent) {
        if (!DataContainer.checkUser(userAgent)) {
            return "redirect:/";
        }
        model.addAttribute("id",DataContainer.getUsersOnline().get(userAgent).getId());
        model.addAttribute("dialogs", MessageDAO.getDialogs(userAgent));
        return "messanger";
    }

    @GetMapping("/messanger/{id}")
    public static String dialog(@PathVariable("id") int id, Model model,@RequestHeader(value = "User-Agent") String userAgent) {
        if (!DataContainer.checkUser(userAgent)) {
            return "redirect:/";
        }
        model.addAttribute("id",DataContainer.getUsersOnline().get(userAgent).getId());
        model.addAttribute("dialog_id",id);
        model.addAttribute("messages",MessageDAO.getDialogMessages(id,userAgent));
        return "dialog";
    }
    @PostMapping("/messanger")
    public static String sendMessage(@RequestParam("message") String message,
                                    @RequestParam("id") String id,
                                    Model model,
                                    @RequestHeader(value = "User-Agent") String userAgent) {
        MessageDAO.createMessage(Integer.parseInt(id),message,userAgent);
        return "redirect:/messanger/" + id;
    }

    @GetMapping("/tech-support")
    public static String techSupport(@RequestHeader(value = "User-Agent") String userAgent) {
        if (!DataContainer.checkUser(userAgent)) {
            return "redirect:/";
        }
        return "tech-support";
    }
    @PostMapping("/tech-support")
    public static String techSupportInfo(HttpServletRequest request) {
        String theme = request.getParameter("theme");
        int owner_id = DataContainer.getUsersOnline().get(request.getHeader("User-Agent")).getId();
        String text = request.getParameter("text");
        String creation_date = new Date().toString();
        MessageDAO.createTechSupportRequest(theme,owner_id,text,creation_date,request.getHeader("User-Agent"));
        return "redirect:/" + owner_id;
    }
}
