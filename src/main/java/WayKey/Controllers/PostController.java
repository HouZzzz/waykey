package WayKey.Controllers;

import WayKey.DAO.PostDAO;
import WayKey.DAO.UserDAO;
import WayKey.DataContainer;
import WayKey.Entities.Comment;
import WayKey.Entities.User;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class PostController {
    private static Logger LOG = LoggerFactory.getLogger(PostController.class);

    @GetMapping("/news")
    public static String news(@RequestParam(required = false) String search,
                              @RequestParam(required = false) String filter,
                              Model model,
                              @RequestHeader(value = "User-Agent") String userAgent) {
        LOG.debug("filter: " + filter);
        if (!DataContainer.checkUser(userAgent)) {
            return "redirect:/";
        }
        User currentUser = DataContainer.getUsersOnline().get(userAgent);
        if (search != null && !search.replaceAll(" ","").isEmpty()) {
            model.addAttribute("id",currentUser.getId());
            model.addAttribute("searchResult", UserDAO.search(search));
            return "search";
        }
        model.addAttribute("id",currentUser.getId());
        model.addAttribute("recommended", PostDAO.getRecommended(filter,userAgent));
        return "news";
    }
    @PostMapping("/like")
    public static String likePost(@RequestBody JSONObject body,@RequestHeader(value = "User-Agent") String userAgent) {
        String id = body.getString("id");
        boolean status = body.getBoolean("status");

        PostDAO.likePost(status,id,userAgent);
        return "redirect:/news";
    }
    @PostMapping("/comment")
    public static String commentPost(@RequestBody JSONObject body,@RequestHeader(value = "User-Agent") String userAgent) {
        String id = body.getString("id");
        String text = body.getString("text").trim();
        PostDAO.commentPost(Integer.parseInt(id),text,userAgent);
        return "redirect:/news";
    }

    @GetMapping("/post/{id}")
    public static String postPage(@PathVariable("id") String id,
                                  Model model,
                                  @RequestHeader(value = "User-Agent") String userAgent) {
        model.addAttribute("id", DataContainer.getUsersOnline().get(userAgent).getId());
        model.addAttribute("post",PostDAO.getPostById(Integer.parseInt(id),userAgent));
        List<Comment> comments = PostDAO.getPostComments(Integer.parseInt(id));
        model.addAttribute("comments",comments);
        return "post";
    }
}
