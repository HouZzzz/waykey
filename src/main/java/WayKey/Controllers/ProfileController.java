package WayKey.Controllers;

import WayKey.DAO.MessageDAO;
import WayKey.DAO.PostDAO;
import WayKey.DAO.UserDAO;
import WayKey.DBconnector;
import WayKey.DataContainer;
import WayKey.Entities.NameRequest;
import WayKey.Entities.User;
import WayKey.Enums.UserRoles;
import WayKey.Servlets.FileUploadServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

@Controller
public class ProfileController {

    private static String timeName,timeLogin,timePassword;
    private static Logger LOG = LoggerFactory.getLogger(PostController.class);
    @GetMapping("/")
    public static String login(@RequestHeader(value = "User-Agent") String userAgent) {
        DataContainer.getUsersOnline().remove(userAgent);
        UserDAO.autoLogin(userAgent);
        if (DataContainer.checkUser(userAgent)) {
            return "redirect:/news";
        }
        return "login";
    }

    @PostMapping("/")
    public static String loginPostReq(@RequestParam("login") String login,
                                      @RequestParam("password") String password,
                                      HttpServletRequest request) {

        switch (UserDAO.loginUser(login,password,request.getHeader("user-agent"),request.getParameter("remember"))) {
            case 200:
                LOG.info("Выполнен вход в аккаунт. " + DataContainer.getUsersOnline().get(request.getHeader("user-agent")));
                return "redirect:/news";
            case 412:
                LOG.error("Попытка входа на аккаунт: ошибка. Неправильный пароль");
                break;
            case 404:
                LOG.error("Попытка входа на аккаунт: ошибка. Аккаунт не найден");
                break;
        }
        return "redirect:/";
    }
    @GetMapping("/reg")
    public static String registration(@RequestParam(required = false) String confirmed,
                                      @RequestHeader(value = "User-Agent") String userAgent){
        DataContainer.getUsersOnline().remove(userAgent);
        if (confirmed != null && confirmed.equals("false")) {
            return "confirm";
        }
        return "registration";
    }

    @PostMapping("/reg")
    public String registrationPostReq(
            Model model,
            @RequestParam("name") String name,
            @RequestParam("login") String login,
            @RequestParam("password") String password,
            @RequestParam("gmail") String gmail) {

        if (password.contains(" ") || !Pattern.compile("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Za-z]).{6,20}$").matcher(password).find()) {
            LOG.info("invalid password");
            model.addAttribute("message","Не валидный пароль!");
            return "registration";
        }

        switch (UserDAO.registerUser(name,login,password,gmail)) {
            case 202:
                timePassword = password;
                timeName = name;
                timeLogin = login;
                return "redirect:/reg?confirmed=false";
            case 302:
                LOG.error("При регистрации пользователя произошла ошибка: Пользователь уже существует");
                break;
            case 500:
                LOG.error("Ошибка в ходе кода");
                break;
        }

        return "redirect:/";
    }

    @PostMapping("/reg/confirm")
    public static String confirm(@RequestParam("code") String code,
                                 @RequestHeader(value = "User-Agent") String userAgent) {
        switch (UserDAO.addUserToDB(timeName,timeLogin,timePassword,code,userAgent)) {
            case 201:
                return "redirect:/news";
            case 412:
                return "redirect:/reg?confirmed=false";
            default:
                return "redirect:/";
        }
    }
    @GetMapping("/{id}")
    public static String profile(@PathVariable("id") int id, Model model,@RequestHeader(value = "User-Agent") String userAgent) throws UnsupportedEncodingException {
        if (!DataContainer.checkUser(userAgent)) {
            return "redirect:/";
        }
        User currentUser = DataContainer.getUsersOnline().get(userAgent);
        User user = UserDAO.getUserObject(id);
        model.addAttribute("friends",UserDAO.getShortFriendList(id));
        model.addAttribute("id",currentUser.getId());
        model.addAttribute("posts",PostDAO.getUserPosts(id,userAgent));
        model.addAttribute("account", UserDAO.getUserObject(id));

        String role = currentUser.getUserRole() == UserRoles.USER ? "USER": "ADMIN";
        model.addAttribute("role",role);
        return "profile";
    }
    @PostMapping("/createPost")
    public static String createPost(HttpServletRequest request) throws ServletException, IOException {
        FileUploadServlet.addPhotoToPost(request);
        return "redirect:/" + DataContainer.getUsersOnline().get(request.getHeader("User-Agent")).getId();
    }
    @PostMapping("/addFriend")
    public static String addFriend(@RequestParam("id") int id,@RequestHeader(value = "User-Agent") String userAgent) {
        UserDAO.addFriend(id,userAgent);
        return "redirect:/" + id;
    }
    @GetMapping("/{id}/edit")
    public static String editPage(@PathVariable("id") String id,Model model,@RequestHeader(value = "User-Agent") String userAgent) {
        User currentUser = DataContainer.getUsersOnline().get(userAgent);
        if (Integer.parseInt(id) != currentUser.getId()) {
            return "redirect:/" + currentUser.getId();
        }
        model.addAttribute("user",currentUser);
        return "edit";
    }
    @PostMapping("/edit")
    public static String edit(HttpServletRequest request) throws ServletException, IOException {
        FileUploadServlet.edit(request);
        return "redirect:/" + DataContainer.getUsersOnline().get(request.getHeader("User-Agent"));
    }
    @PostMapping("/logout")
    public static String logout(@RequestHeader(value = "User-Agent") String userAgent) {
        UserDAO.logout(userAgent);
        DataContainer.getUsersOnline().remove(userAgent);
        return "redirect:/";
    }

    @GetMapping("/admin")
    public static String adminPage(@RequestHeader(value = "User-Agent") String userAgent) {
        User currentUser = DataContainer.getUsersOnline().get(userAgent);
        if (currentUser.getUserRole() != UserRoles.ADMIN) {
            return "redirect:/" + currentUser.getId();
        }
        return "admin";
    }

    @GetMapping("/admin/nameRequests")
    public static String nameRequests(Model model) {
        try {
            Connection connection = DBconnector.getConnection();
            Statement statement = connection.createStatement();

            ResultSet resultSet = statement.executeQuery("SELECT * FROM name_change_requests");

            List<NameRequest> requests = new ArrayList<>();

            while (resultSet.next()) {
                NameRequest request = new NameRequest();
                request.setId(resultSet.getInt("id"));
                request.setRequest_owner_id(resultSet.getInt("request_owner_id"));
                request.setOld_name(resultSet.getString("old_name"));
                request.setNew_name(resultSet.getString("new_name"));
                requests.add(request);
            }
            model.addAttribute("requests",requests);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return "nameRequests";
    }
    @PostMapping("/admin/nameRequests")
    public static String todoGetter(@RequestParam("id") String id,@RequestParam("todo") String todo, @RequestParam(required = false) String name) {

        switch (todo) {
            case "accept":
                UserDAO.acceptName(id,name);
                break;
            case "reject":
                UserDAO.rejectName(id);
                break;
        }
        return "redirect:/admin/nameRequests";
    }
    @GetMapping("/admin/techSupport")
    public static String techSupport(Model model,HttpServletRequest request) {
        model.addAttribute("requests", MessageDAO.getTechSupportRequests());
        return "admin-tech-support";
    }
    @PostMapping("/admin/techSupport")
    public static String answerToRequest(HttpServletRequest request){
        String requestID = request.getParameter("id");
        String ownerID = request.getParameter("owner_id");
        String text = request.getParameter("text");
        MessageDAO.answerToTechSupportRequest(Integer.parseInt(requestID),Integer.parseInt(ownerID),text,request.getHeader("User-Agent"));
        return "redirect:/admin/techSupport";
    }
}
