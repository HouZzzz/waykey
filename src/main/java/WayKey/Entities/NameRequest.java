package WayKey.Entities;

public class NameRequest {
    private int id;
    private int request_owner_id;
    private String old_name;
    private String new_name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRequest_owner_id() {
        return request_owner_id;
    }

    public void setRequest_owner_id(int request_owner_id) {
        this.request_owner_id = request_owner_id;
    }

    public String getOld_name() {
        return old_name;
    }

    public void setOld_name(String old_name) {
        this.old_name = old_name;
    }

    public String getNew_name() {
        return new_name;
    }

    public void setNew_name(String new_name) {
        this.new_name = new_name;
    }
}
