package WayKey;

import WayKey.Entities.User;

import java.util.HashMap;
import java.util.Map;

public class DataContainer {
    private static Map<String,User> usersOnline = new HashMap<>();

    public static Map<String, User> getUsersOnline() {
        return usersOnline;
    }
    public static boolean checkUser(String userAgent) {
        return usersOnline.containsKey(userAgent);
    }
}
