package WayKey.DAO;

import WayKey.Configuration.Config;
import WayKey.DBconnector;
import WayKey.DataContainer;
import WayKey.Entities.Message;
import WayKey.Entities.TechSupportRequest;
import WayKey.Entities.User;
import WayKey.Logic.MessageEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MessageDAO {
    private static Logger LOG = LoggerFactory.getLogger(MessageDAO.class);

    /**
    Создание сообщения
    @param getter_id id аккаунта получателя сообщения
    @param text текст сообщения
     */
    public static byte createMessage(int getter_id,String text,String userAgent) {
        try {
            Connection connection = DBconnector.getConnection();
            Statement statement = connection.createStatement();

            String encodedMessage = MessageEncoder.encodeMessage(text);
            LOG.debug("encoded message: " + encodedMessage);

            User currentUser = DataContainer.getUsersOnline().get(userAgent);
            statement.execute(String.format("INSERT INTO messages (sender_id, getter_id, text) VALUES (%d, %d, '%s')",currentUser.getId(),getter_id,encodedMessage));
            LOG.info("Отправлено сообщение: Получатель -> " + getter_id + ", отправитель -> " + currentUser);
            return 1; // все прошло успешно

        } catch (SQLException e) {
            throw new RuntimeException("Ошибка при подключении к базе данных");
        }
    }

    /**
     * получение всех диалогов пользователя
     * @return список пользователей с которым у пользователя есть диалог
     */
    public static List<User> getDialogs(String userAgent) {
        List<User> dialogs = new ArrayList<>();

        try {
            Connection connection = DBconnector.getConnection();
            Statement statement = connection.createStatement();

            User currentUser = DataContainer.getUsersOnline().get(userAgent);

            int id = currentUser.getId();
            ResultSet dialogsRS = statement.executeQuery(String.format("SELECT DISTINCT getter_id,sender_id FROM messages WHERE sender_id = %d OR getter_id = %d ORDER BY id DESC", id,id));
            User dialog;
            while (dialogsRS.next()) {
                if (dialogsRS.getInt("getter_id") != currentUser.getId()) {
                    dialog = UserDAO.getUserObject(dialogsRS.getInt("getter_id"));
                } else {
                    dialog = UserDAO.getUserObject(dialogsRS.getInt("sender_id"));
                }
                if (dialog.getId() != 0) {
                    if (dialog.getId() == currentUser.getId()) {
                        dialog.setPhoto(getFavouritesImg());
                        dialog.setName("Избранное");
                    }
                    dialogs.add(dialog);
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException("Ошибка при подключении к базе данных");
        }
        LOG.debug("Диалоги успешно найдены");
        dialogs = dialogs
                .stream()
                .distinct()
                .collect(Collectors.toList());
        return dialogs;
    }

    /**
     * получение всех сообщений в диалоге с пользователем
     * @param dialog_id id пользователя, диалог с которым нужно найти
     * @return список сообщений
     */
    public static List<Message> getDialogMessages(int dialog_id,String userAgent) {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(Config.class);

        List<Message> messages = new ArrayList<>();

        try {
            Connection connection = DBconnector.getConnection();
            Statement statement = connection.createStatement();

            int currentUserID = DataContainer.getUsersOnline().get(userAgent).getId();

            ResultSet messagesRS = statement.executeQuery(String.format(
                    "SELECT u.name,m.text,m.sender_id FROM messages AS m JOIN users AS u ON m.sender_id = u.id " +
                            "WHERE sender_id = %d AND getter_id = %d OR sender_id = %d AND getter_id = %d ORDER BY m.id DESC",
                    dialog_id,currentUserID,currentUserID,dialog_id));
            while (messagesRS.next()) {
                Message message = context.getBean("messageBean",Message.class);
                message.setSenderName(messagesRS.getString("name"));
                message.setText(MessageEncoder.decodeMessage(messagesRS.getString("text")));
                message.setSenderPhoto(UserDAO.getUserObject(messagesRS.getInt("sender_id")).getPhoto());
                messages.add(message);
                LOG.debug("text: " + message.getText() + ", sender: " + message.getSenderName());
            }
        } catch (SQLException e) {
            throw new RuntimeException("Ошибка при подключении к базе данных");
        }
        context.close();
        return messages;
    }

    /**
     * @return иконка избранных сообщений в формате base64
     */
    private static String getFavouritesImg() {
        return "iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAABmJLR0QA/wD/AP+gvaeTAAACiUlEQVRoge2Zu2sUURTGfz7AjQ8wm1SJWPsAC0GwSArFNgS0SKnRzu2DSRo7/wIV7MUomMRaJLUGtQpuEAuzSedjTYIum5VYnBt3cp2ZO/fO3Bkh88FhYfZ855xv5tw75+5CiRIl9iSGgRlgDegA247WAVaBJ8BQHoUfBB6mKNhk91UOb/BZfFCEFwznUPyOeWmnmRwFPPYhYC1HAatJi9pnIaADHLDwT4PfJFzMNgK23WpxRqLa9vuuwjdKAUWjFFA0SgFFoxRQNLzO3gYsA/PAS2TOaiAjRBU46yNhVoPaInDZR4EmpC28DdTozjhV4CbyFOrAprI6MAeMA73/i4BvwCUVpweYBn4k4DWBKcUpTECbbssMAG8cYrwHThYloKb4J5CDimucBjCYt4BFpOd7gLcxfi3gorKWIV4lTwE7rTNt8JsN5Jk1+N7JS0Bd8aqYF+xYIM+YwbeJ4+5kK+Ce4t0y+G0CRwJ5DgMbBs6NPARcUbx57fpVh9zXtBh/W87nLLSiPk9r15ccYumcMw4xrJ/AUcXT2+EzstskxXngkxZjw7eAX3QHxfWQ71vA7QQ5a4Rvq+s+BSwB5wK85RjfkZh8IzG8egwvlYAH/Du3zEX4bgF9Mfn6lE8Y93nWAr4AoxG88QjOgubXryyIhQju9SwFvCJ+RqkiLx+dF3yjDiGzToPdP61PhvC+A8ezENBWRSTZiqdC+E+ROz7J7lbZUtf6gWchvAmX4gF+BoJ8BC5YcCvIIGa7Fev2mhTD3F3gK/AIOObAH0RaxLX4FeQ8USgGkLtoW/w7MjjQZIUKsnbCFnbYgp0ADkUFs/mDI2v0IlvvKHAKObGBnNo+AC+UNQuprkRC/AHFXowFADPvtQAAAABJRU5ErkJggg==";
    }

    /**
     * создание запроса в тех поддержку
     * @param theme тема обращения в тех поддержку
     * @param owner_id id отправителя запроса
     * @param text текст запроса
     * @param creation_date дата запроса в формате Wed Sep 07 10:44:48 MSK 2022
     */
    public static void createTechSupportRequest(String theme, int owner_id,String text,
                                                String creation_date,
                                                String userAgent){
        LOG.debug(theme);
        LOG.debug("" + owner_id);
        LOG.debug(text);
        LOG.debug(creation_date);

        try {
            Connection connection = DBconnector.getConnection();
            Statement statement = connection.createStatement();

            statement.execute(String.format("INSERT INTO tech_support (theme, owner_id, text, creation_date) VALUES ('%s',%d,'%s','%s')",
                    theme,owner_id,text,creation_date));
            createMessage(1,text,userAgent);

            User currentUser = DataContainer.getUsersOnline().get(userAgent);
            int id = currentUser.getId();
            currentUser.setId(1);
            createMessage(id,"Ваша проблема отправилась на рассмор команде WK! Ожидайте ответа.",userAgent);
            currentUser.setId(id);
            LOG.info("Успешно создано сообщение в тех поддержку");
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Ошибка при подключении к базе данных");
        }
    }

    /**
     * @return список всех запросов в тех поддержку (доступно только администратором сайта)
     */
    public static List<TechSupportRequest> getTechSupportRequests() {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(Config.class);

        List<TechSupportRequest> requests = new ArrayList<>();

        try {
            Connection connection = DBconnector.getConnection();
            Statement statement = connection.createStatement();

            ResultSet requestsRS = statement.executeQuery("SELECT * FROM tech_support");

            while (requestsRS.next()) {
                TechSupportRequest request = context.getBean("techSupportRequestBean",TechSupportRequest.class);

                request.setId(requestsRS.getInt("id"));
                request.setTheme(requestsRS.getString("theme"));
                request.setOwner_id(requestsRS.getInt("owner_id"));
                request.setText(requestsRS.getString("text"));

                requests.add(request);
            }
        } catch (SQLException e) {
            throw new RuntimeException("Ошибка при подключении к базе данных");
        }
        context.close();
        return requests;
    }

    /**
     * ответ на запрос в тех поддержку
     * @param requestID айди запроса
     * @param owner_id айди отправителя запроса
     * @param text текст ответа на запрос
     */
    public static void answerToTechSupportRequest(int requestID,int owner_id, String text,String userAgent) {
        LOG.debug(requestID + "");
        LOG.debug(owner_id + "");
        LOG.debug(text);

        try {
            Connection connection = DBconnector.getConnection();
            Statement statement = connection.createStatement();

            User currentUser = DataContainer.getUsersOnline().get(userAgent);

            int id = currentUser.getId();
            currentUser.setId(1);
            createMessage(owner_id,text,userAgent);
            currentUser.setId(id);
            statement.executeUpdate("UPDATE tech_support SET status = 'Закрыто' WHERE id = " + requestID);
            LOG.info("Ответ на запрос в тех поддержку был успешн создан");
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Ошибка при подключении к базе данных");
        }
    }
}
