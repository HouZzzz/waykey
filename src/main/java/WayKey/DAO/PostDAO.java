package WayKey.DAO;

import WayKey.Configuration.Config;
import WayKey.DBconnector;
import WayKey.DataContainer;
import WayKey.Entities.Comment;
import WayKey.Entities.Post;
import WayKey.Entities.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class PostDAO {
    private static Logger LOG = LoggerFactory.getLogger(PostDAO.class);


    /**
     * создание поста
     * @param postText текст поста
     * @param photo фото поста (не обязательно)
     */
    public static void createPost(String postText, Part photo,String userAgent) {
        try {
            Connection connection = DBconnector.getConnection();
            Statement statement = connection.createStatement();

            if(photo.getSize() != 0) {
                UploadPhoto(photo,postText,userAgent);
            } else {
                User currentUser = DataContainer.getUsersOnline().get(userAgent);
                statement.execute(String.format("INSERT INTO posts (text,owner_id,date) VALUES ('%s',%d,'%s')",
                        postText,
                        currentUser.getId(),
                        new Date().toString()));
                LOG.info("Создан новый пост. ID создателя -> " + currentUser.getId());
            }
        } catch (SQLException e) {
            throw new RuntimeException("Ошибка при подключении к базе данных");
        }
    }

    /**
     * получение всех постов пользователя
     * @param id id пользователя посты которого нужно найти
     * @return список постов пользователя
     */
    public static List<Post> getUserPosts(int id,String userAgent) {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(Config.class);

        List<Post> userPosts = new ArrayList<>();
        try {
            Connection connection = DBconnector.getConnection();
            Statement statement = connection.createStatement();

            ResultSet postsRS = statement.executeQuery(String.format("SELECT * FROM posts WHERE owner_id = %d ORDER BY id DESC",id));
            while (postsRS.next()) {
                Post post = context.getBean("postBean",Post.class);
                User owner = UserDAO.getUserObject(postsRS.getInt("owner_id"));
                post.setId(postsRS.getInt("id"));
                post.setText(postsRS.getString("text"));
                post.setLikes_id(postsRS.getString("likes_id"));
                post.setOwner_id(postsRS.getInt("owner_id"));
                post.setOwnerName(owner.getName());
                post.setOwnerPhoto(owner.getPhoto());
                post.setLiked(postsRS.getString("likes_id").contains("-" + String.valueOf(DataContainer.getUsersOnline().get(userAgent).getId()) + "-"));
                post.setLikeCounter(Post.countLikes(post));
                post.setDate(stringifyDate(postsRS.getString("date")));
                if (LOG.isDebugEnabled()) {
                    System.out.println("----------------------------------------------------------------------");
                    System.out.println("----------------------------------------------------------------------");
                }
                LOG.debug("id: " + post.getId());
                LOG.debug("text: " + post.getText());
                LOG.debug("likes_id: " + post.getLikes_id());
                LOG.debug("owner_id: " + post.getOwner_id());
                LOG.debug("owner_name: " + post.getOwnerName());
                LOG.debug("liked: " + post.isLiked());
                LOG.debug("like counter: " + post.getLikeCounter());
                LOG.debug("date: " + postsRS.getString("date"));

                Blob photoBlob = postsRS.getBlob("post_photo");
                LOG.debug("post have photo -> " + (photoBlob != null));
                if (photoBlob != null) {
                    byte[] encoded = Base64.getEncoder().encode(photoBlob.getBinaryStream().readAllBytes());
                    String photo = new String(encoded,"UTF-8");
                    post.setPhoto(photo);
                }
                userPosts.add(post);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Ошибка при подключении к базе данных");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        context.close();
        return userPosts;
    }

    /**
     * получение рекомендованных постов
     * @param filter фильтрация постов
     * @return список рекомендруемых постов
     */
    public static List<Post> getRecommended(String filter,String userAgent) {
        User currentUser = DataContainer.getUsersOnline().get(userAgent);
        if (currentUser.getFriends_id().isEmpty()) {
            return null;
        }
        String[] usersFriends = currentUser.getFriends_id().split("-");

        List<Post> recommendedPosts = new ArrayList<>();

        for (String friend : usersFriends) {
            if (friend.isEmpty()) {
                continue;
            }
            getUserPosts(Integer.parseInt(friend),userAgent).forEach(recommendedPosts::add);
        }
        if (filter != null) {
            switch (filter) {
                case "newest":
                    recommendedPosts = recommendedPosts.stream()
                            .sorted((p1,p2) -> Integer.compare(p2.getId(),p1.getId()))
                            .collect(Collectors.toList());
                    break;
                case "most_popular":
                    recommendedPosts = recommendedPosts.stream()
                            .sorted(((p1, p2) -> Integer.compare(p2.getLikeCounter(),p1.getLikeCounter())))
                            .collect(Collectors.toList());
                    break;
            }
        } else {
            recommendedPosts = recommendedPosts.stream()
                    .sorted(((p1, p2) -> Integer.compare(p2.getLikeCounter(),p1.getLikeCounter())))
                    .collect(Collectors.toList());
        }

        return recommendedPosts;
    }

    /**
     * поставить/убрать лайк с поста
     * @param status false/true лайкнут ли пост
     * @param id id поста на котором надо изменить отметку
     */
    public static void likePost(boolean status, String id, String userAgent) {
        try {
            Connection connection = DBconnector.getConnection();
            Statement statement = connection.createStatement();

            ResultSet likesRS = statement.executeQuery("SELECT likes_id FROM posts WHERE id = " + id);
            likesRS.next();
            User currentUser = DataContainer.getUsersOnline().get(userAgent);
            if (status) {
                String likes = likesRS.getString("likes_id");

                String newLikes = likes += ("-" + currentUser.getId() + "-");

                statement.executeUpdate(String.format("UPDATE posts SET likes_id = '%s' WHERE id = %d",newLikes,Integer.parseInt(id)));

                LOG.info("Пользователь " + currentUser.getId() + " лайкнул пост " + id);
            } else {
                String likes = likesRS.getString("likes_id");
                String newLikes = likes.replaceAll(
                        ("-" + currentUser.getId() + "-"),"");
                statement.executeUpdate(String.format("UPDATE posts SET likes_id = '%s' WHERE id = %d",newLikes,Integer.parseInt(id)));

                LOG.info("Пользователь " + currentUser.getId() + " убрал лайк с поста " + id);
            }
        } catch (SQLException e) {
            throw new RuntimeException("Ошибка при подключении к базе данных");
        }
    }

    /**
     * оставить комментарий под постом
     * @param id id поста под котором нажно записать комментарий
     * @param text текст комментария
     */
    public static void commentPost(int id, String text,String userAgent) {
        if (!text.replaceAll("\n","").isEmpty()) {
            try {
                Connection connection = DBconnector.getConnection();
                Statement statement = connection.createStatement();

                statement.execute(String.format("INSERT INTO comments (text,owner_id,post_id) VALUES ('%s',%d,%d)",
                        text,
                        DataContainer.getUsersOnline().get(userAgent).getId(),
                        id));
                LOG.debug("comment text: " + text);
                LOG.info("К посту " + id + " был добавлен комментарий");
            } catch (SQLException e){
                e.printStackTrace();
                throw new RuntimeException("Ошибка при подключении к базе данных");
            }
        }
    }

    /**
     * получить комментарии поста
     * @param postID id поста комментарии которого надо найти
     * @return список комментариев
     */
    public static List<Comment> getPostComments(int postID) {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(Config.class);

        List<Comment> comments = new ArrayList<>();

        try {
            Connection connection = DBconnector.getConnection();
            Statement statement = connection.createStatement();

            ResultSet commentsRS = statement.executeQuery("SELECT * FROM comments WHERE post_id = " + postID + " ORDER BY id DESC");

            while (commentsRS.next()) {
                Comment comment = context.getBean("commentBean",Comment.class);
                comment.setId(commentsRS.getInt("id"));
                comment.setOwner_id(commentsRS.getInt("owner_id"));
                comment.setOwnerName(UserDAO.getUserObject(comment.getOwner_id()).getName());
                comment.setPostID(postID);
                comment.setText(commentsRS.getString("text"));
                comment.setOwnerPhoto(UserDAO.getUserObject(comment.getOwner_id()).getPhoto());
                comments.add(comment);

                LOG.debug("id: " + comment.getId());
                LOG.debug("owner_id: " + comment.getOwner_id());
                LOG.debug("owner_name: " + comment.getOwnerName());
                LOG.debug("post_id: " + comment.getPostID());
                LOG.debug("text: " + comment.getText());
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Ошибка при подключении к базе данных");
        }
        context.close();
        return comments;
    }

    /**
     * получить пост по его id
     * @param id id поста который нужно найти
     * @return пост найденный по id
     */
    public static Post getPostById(int id,String userAgent) {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(Config.class);

        Post post  = context.getBean("postBean",Post.class);

        try {
            Connection connection = DBconnector.getConnection();
            Statement statement = connection.createStatement();

            ResultSet postRS = statement.executeQuery("SELECT * FROM posts WHERE id = " + id);
            postRS.next();

            post.setId(postRS.getInt("id"));
            post.setLikes_id(postRS.getString("likes_id"));
            post.setText(postRS.getString("text"));
            post.setOwner_id(postRS.getInt("owner_id"));

            String ownerName = UserDAO.getUserObject(post.getOwner_id()).getName();
            String ownerPhoto = UserDAO.getUserObject(post.getOwner_id()).getPhoto();
            post.setOwnerName(ownerName);
            post.setOwnerPhoto(ownerPhoto);
            post.setLiked(postRS.getString("likes_id").contains("-" + String.valueOf(DataContainer.getUsersOnline().get(userAgent).getId()) + "-"));
            post.setLikeCounter(Post.countLikes(post));
            post.setDate(stringifyDate(postRS.getString("date")));

            Blob photoBlob = postRS.getBlob("post_photo");
            if (photoBlob != null) {
                byte[] encoded = Base64.getEncoder().encode(photoBlob.getBinaryStream().readAllBytes());
                String photo = new String(encoded,"UTF-8");
                post.setPhoto(photo);
            }
            if (LOG.isDebugEnabled()) {
                System.out.println("----------------------------------------------------------------------");
                System.out.println("----------------------------------------------------------------------");
            }
            LOG.debug("id: " + post.getId());
            LOG.debug("likes_id: " + post.getLikes_id());
            LOG.debug("text: " + post.getText());
            LOG.debug("owner_id: " + post.getOwner_id());
            LOG.debug("owner_name: " + post.getOwnerName());
            LOG.debug("liked: " + post.isLiked());
            LOG.debug("like counter: " + post.getLikeCounter());
            LOG.debug("post have photo: " + (photoBlob != null));
        }catch (SQLException e){
            e.printStackTrace();
            throw new RuntimeException("Ошибка при подключении к базе данных");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        context.close();
        return post;
    }

    /**
     * загрузить фото в базу данных
     * @param filePart фото
     * @param text текст поста
     */
    public static short UploadPhoto(Part filePart, String text,String userAgent) {
        try {
            InputStream inputStream = filePart.getInputStream();

            Connection connection = DBconnector.getConnection();
            String sql = "INSERT INTO posts (post_photo,owner_id,text) values (?,?,?)";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setBlob(1,inputStream);
            statement.setInt(2,DataContainer.getUsersOnline().get(userAgent).getId());
            statement.setString(3,text);

            int row = statement.executeUpdate();
            if (row > 0) {
                double fileSize = filePart.getSize();
                if (fileSize / 1024 < 1024) {
                    LOG.info("В базу данных успешно загружена фотография поста. Размер файла: " + (int) (fileSize / 1024) + " KiB");
                } else {
                    LOG.info("В базу данных успешно загружена фотография поста. Размер файла: " + String.format("%.2f",((fileSize / 1024) / 1024)) + " MB");
                }
                return 201; // 201 created
            } else {
                return 500; // 500 internal server error
            }
        } catch (SQLException e){
            e.printStackTrace();
            throw new RuntimeException("Ошибка при подключении к базе данных");
        } catch (IOException e) {}
        return 500; // 500 internal server error
    }
    private static String stringifyDate(String dateString) {
        // разделяем даты на составные
        String[] date = dateString.split(" ");
        String[] currentDate = new Date().toString().split(" ");

        // создаем константы для облегчения вычисления и читабельности
        int minutesInYear = 525960;
        int minutesInMonth = 43800;
        int minutesInDay = 1440;
        int minutesInHour = 60;

        long minutesPassed = 0
                + Integer.parseInt(currentDate[3].split(":")[1]) - Integer.parseInt(date[3].split(":")[1]) // минуты
                + (minutesInHour * (Integer.parseInt(currentDate[3].split(":")[0]) - Integer.parseInt(date[3].split(":")[0]))) // часы
                + (minutesInDay * (Integer.parseInt(currentDate[2]) - Integer.parseInt(date[2]))) // дни
                + (minutesInMonth * (getMonth(currentDate[1]) - getMonth(date[1]))) // месяцы
                + (minutesInYear * (Integer.parseInt(currentDate[5]) - Integer.parseInt(date[5]))); // года

        // выдаем результат в зависимости от случая
        if (minutesPassed < minutesInHour) return minutesPassed != 0 ? minutesPassed + " минут назад" : "только что";// минуты
        else if (minutesPassed < minutesInDay) return minutesPassed > (minutesInHour * 2) ? (minutesPassed / minutesInHour) + " часов назад" : "час назад";// часы
        else if (minutesPassed < minutesInMonth) return minutesPassed > (minutesInDay * 2) ? (minutesPassed / minutesInDay) + " дней назад" : "вчера";// дни
        else if (minutesPassed < minutesInYear) return minutesPassed > (minutesInMonth * 2) ? (minutesPassed / minutesInMonth) + " месяцев назад" : "месяц назад";// месяцы
        else return minutesPassed > (minutesInYear * 2) ? (minutesPassed / minutesInYear) + " лет назад" : "год назад";// годы
    }
    private static int getMonth(String month) {
        switch (month.toLowerCase()) {
            case "jan" -> {
                return 1;
            }
            case "feb" -> {
                return 2;
            }
            case "mar" -> {
                return 3;
            }
            case "apr" -> {
                return 4;
            }
            case "may" -> {
                return 5;
            }
            case "jun" -> {
                return 6;
            }
            case "jul" -> {
                return 7;
            }
            case "aug" -> {
                return 8;
            }
            case "sep" -> {
                return 9;
            }
            case "oct" -> {
                return 10;
            }
            case "nov" -> {
                return 11;
            }
            case "dec" -> {
                return 12;
            }
        }
        return 0;
    }
}
