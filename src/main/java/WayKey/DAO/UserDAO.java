package WayKey.DAO;

import WayKey.Configuration.Config;
import WayKey.DBconnector;
import WayKey.DataContainer;
import WayKey.Entities.User;
import WayKey.Enums.UserRoles;
import WayKey.Logic.KeyCreator;
import WayKey.Logic.MailSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

public class UserDAO {
    private static Logger LOG = LoggerFactory.getLogger(UserDAO.class);
    private static String confirmCode = "wkDefaulConfirmCode";

    /**
     * регистрация пользоватля
     * @param name имя нового пользователя
     * @param login логин нового пользователя (должен быть уникальный)
     * @param password пароль нового пользователя
     * @param gmail почта нового пользователя для отправки кода подтверждения
     */
    public static short registerUser(String name, String login, String password,String gmail) {
        try {
            Connection connection = DBconnector.getConnection();
            Statement statement = connection.createStatement();
            LOG.debug("name: " + name);
            LOG.debug("login: " + login);
            LOG.debug("password: " + password);
            LOG.debug("gmail: " + gmail);

            // проверка на существование аккаунта
            if (!statement.executeQuery(String.format("SELECT id FROM users WHERE login = '%s'", login)).next()) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        confirmCode = KeyCreator.generate4dKey();
                        LOG.info("confirm code: " + confirmCode);
                        MailSender.sendMail(gmail,"Подтверждение аккаунта","Ваш код подтверждения: " + confirmCode);
                    }
                }).start();
                return 202; // пользователя еще нету в базе данных (202 accepted)
            } else {
                return 302; // аккаунт существует (302 Found)
            }
        } catch (SQLException e) {
            throw new RuntimeException("Ошибка при подключении к базе данных");
        }
    }

    /**
     * добавление пользователя в базу данных
     * @param name имя нового пользователя
     * @param login логин нового пользователя (должен быть уникальный)
     * @param password пароль нового пользователя
     * @param code код подтверждения пришедший на почту
     */
    public static short addUserToDB(String name, String login, String password, String code,String userAgent) {
        LOG.debug("name: " + name);
        LOG.debug("login: " + login);
        LOG.debug("password: " + password);
        LOG.debug("code: " + code);
        try {
            Connection connection = DBconnector.getConnection();
            Statement statement = connection.createStatement();

            if (code.equals(confirmCode)) {
                statement.execute(String.format("INSERT INTO users (name, login, password) VALUES ('%s', '%s', '%s')", name, login, password));
                DataContainer.getUsersOnline().put(userAgent,getUserObject(String.format("SELECT * FROM users WHERE login = '%s'", login)));
                LOG.info(String.format("Зарегестритован новый пользователь. Имя: %s, логин: %s, пароль: %s",name,login,password));
                return 201; // регистрация прошла успешно (201 created)
            } else {
                LOG.error("Ошибка при регистрации аккаунта: неверный код подтверждения");
                return 412; // неверный код подтверждения (412 precondition failed)
            }
        } catch (SQLException e) {
            throw new RuntimeException("Ошибка при подключении к базе данных");
        }
    }

    /**
     * войти в аккаунт
     * @param login логин пользователя
     * @param password пароль пользователя
     * @param userAgent информация о браузере пользователя для его запоминания по желанию
     * @param remember true/false запомнить ли пользователя в этом браузере
     */
    public static short loginUser(String login, String password, String userAgent, String remember) {
        LOG.debug("login: " + login);
        LOG.debug("password: " + password);
        LOG.debug("user agent: " + userAgent);
        LOG.debug("remember: " + remember);

        DataContainer.getUsersOnline().put(userAgent,getUserObject(String.format("SELECT * FROM users WHERE login = '%s'", login)));
        User currentUser = DataContainer.getUsersOnline().get(userAgent);

        try {
            Connection connection = DBconnector.getConnection();
            Statement statement = connection.createStatement();
            ResultSet roleRS = statement.executeQuery("SELECT role FROM users WHERE login = '" + login + "'");
            roleRS.next();
            String role = roleRS.getString("role");
            currentUser.setUserRole(role.equals("USER") ? UserRoles.USER : UserRoles.ADMIN);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Ошибка при подключении к базе данных");
        }

        if (currentUser.getPassword() != null) {
            if (currentUser.getPassword().equals(password)) {
                    if (remember != null && remember.equals("true")) {
                        try {
                            Connection connection = DBconnector.getConnection();
                            Statement statement = connection.createStatement();

                            statement.executeUpdate(String.format("UPDATE users SET user_agent = '%s' WHERE login = '%s'",userAgent,login));
                        } catch (SQLException e) {
                            throw new RuntimeException("Ошибка при подключении к базе данных");
                        }
                    }
                return 200; // вход в аккаунт прошел успешно (200 ok)
            } else {
                DataContainer.getUsersOnline().remove(userAgent);
                return 412; // пароль не совпадает (412 precondition failed)
            }
        } else {
            return 404; // аккаунт не найден (404 not found)
        }
    }

    /**
     * получить объект пользователя по своему sql запросу
     * @param sql запрос в базу данных
     * @return найденый объект пользователя
     */
    public static User getUserObject(String sql) {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(Config.class);

        User user = context.getBean("userBean",User.class);
        try {
            Connection connection = DBconnector.getConnection();
            Statement statement = connection.createStatement();

            ResultSet usersRS = statement.executeQuery(sql);

            while (usersRS.next()) {
                user.setId(usersRS.getInt("id"));
                user.setName(usersRS.getString("name"));
                user.setStatus(usersRS.getString("status"));
                user.setFriends_id(usersRS.getString("friends_id"));
                user.setGroups_id(usersRS.getString("groups_id"));
                user.setLogin(usersRS.getString("login"));
                user.setPassword(usersRS.getString("password"));

                LOG.debug("id: " + user.getId());
                LOG.debug("name: " + user.getName());
                LOG.debug("status: " + user.getStatus());
                LOG.debug("friends_id: " + user.getFriends_id());
                LOG.debug("groups_id: " + user.getGroups_id());
                LOG.debug("login: " + user.getLogin());
                LOG.debug("password: " + user.getPassword());
            }

        } catch (SQLException e) {
            throw new RuntimeException("Ошибка при подключении к базе данных");
        }
        context.close();
        return user;
    }

    /**
     * получить объект пользователя по его id
     * @param id id пользователя которого нужно найти
     * @return найденый объект пользователя
     */
    public static User getUserObject(int id) {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(Config.class);

        User user = context.getBean("userBean",User.class);

        try {
            Connection connection = DBconnector.getConnection();
            Statement statement = connection.createStatement();

            ResultSet usersRS = connection.createStatement().executeQuery(String.format("SELECT * FROM users WHERE id = %d", id));

            while (usersRS.next()) {
                user.setId(usersRS.getInt("id"));
                user.setName(usersRS.getString("name"));
                user.setStatus(usersRS.getString("status"));
                user.setFriends_id(usersRS.getString("friends_id"));
                user.setGroups_id(usersRS.getString("groups_id"));
                user.setLogin(usersRS.getString("login"));
                user.setPassword(usersRS.getString("password"));

                ResultSet photoRS = statement.executeQuery("SELECT photo FROM profile_pictures WHERE owner_id = " + id + " ORDER BY id DESC LIMIT 1");
                while (photoRS.next()) {
                    byte[] encoded = Base64.getEncoder().encode(photoRS.getBlob("photo").getBinaryStream().readAllBytes());
                    String photo = new String(encoded,"UTF-8");
                    user.setPhoto(photo);
                }
                LOG.debug("id: " + user.getId());
                LOG.debug("name: " + user.getName());
                LOG.debug("status: " + user.getStatus());
                LOG.debug("friends_id: " + user.getFriends_id());
                LOG.debug("groups_id: " + user.getGroups_id());
                LOG.debug("login: " + user.getLogin());
                LOG.debug("password: " + user.getPassword());
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Ошибка при подключении к базе данных");
        } catch (IOException e) {
        }
        context.close();
        return user;
    }

    /**
     * получить объект пользователя по значению в таблице
     * @param column название колонки в которой нужно искать значение
     * @param value искомое значение
     * @return найденый объект пользователя
     */
    public static User getUserObject(String column, String value) {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(Config.class);

        User user = context.getBean("userBean",User.class);

        try {
            Connection connection = DBconnector.getConnection();
            Statement statement = connection.createStatement();

            ResultSet usersRS = statement.executeQuery(String.format("SELECT * FROM users WHERE %s = '%s'", column, value));

            while (usersRS.next()) {
                user.setId(usersRS.getInt("id"));
                user.setName(usersRS.getString("name"));
                user.setStatus(usersRS.getString("status"));
                user.setFriends_id(usersRS.getString("friends_id"));
                user.setGroups_id(usersRS.getString("groups_id"));
                user.setLogin(usersRS.getString("login"));
                user.setPassword(usersRS.getString("password"));

                LOG.debug("id: " + user.getId());
                LOG.debug("name: " + user.getName());
                LOG.debug("status: " + user.getStatus());
                LOG.debug("friends_id: " + user.getFriends_id());
                LOG.debug("groups_id: " + user.getGroups_id());
                LOG.debug("login: " + user.getLogin());
                LOG.debug("password: " + user.getPassword());
            }
        } catch (SQLException e) {
            throw new RuntimeException("Ошибка при подключении к базе данных");
        }
        context.close();
        return user;
    }

    /**
     * получение списока всех пользователей
     * @return список всех пользователей
     */
    public static List<User> getUsersList() {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(Config.class);

        List<User> users = new ArrayList<>();

        try {
            Connection connection = DBconnector.getConnection();
            Statement statement = connection.createStatement();

            ResultSet usersRS = statement.executeQuery("SELECT * FROM users");

            while (usersRS.next()) {
                User user = context.getBean("userBean",User.class);

                user.setId(usersRS.getInt("id"));
                user.setName(usersRS.getString("name"));
                user.setStatus(usersRS.getString("status"));
                user.setFriends_id(usersRS.getString("friends_id"));
                user.setGroups_id(usersRS.getString("groups_id"));
                user.setLogin(usersRS.getString("login"));
                user.setPassword(usersRS.getString("password"));

                LOG.debug("id: " + user.getId());
                LOG.debug("name: " + user.getName());
                LOG.debug("status: " + user.getStatus());
                LOG.debug("friends_id: " + user.getFriends_id());
                LOG.debug("groups_id: " + user.getGroups_id());
                LOG.debug("login: " + user.getLogin());
                LOG.debug("password: " + user.getPassword());

                users.add(user);
            }
        } catch (SQLException e) {
            throw new RuntimeException("Ошибка при подключении к базе данных");
        }
        context.close();
        return users;
    }

    /**
     * добавление пользователя в друзья
     * @param id id пользователя которому нужно отправить запрос в друзья
     */
    public static short addFriend(int id,String uesrAgent) {
        User currentUser = DataContainer.getUsersOnline().get(uesrAgent);

        if (id == currentUser.getId()) {
            LOG.error("Ошибка при добавлении в друзья: запрос самому себе");
            return 403; // 403 forbidden
        }
        LOG.debug("request to " + id);

        try {
            Connection connection = DBconnector.getConnection();
            Statement statement = connection.createStatement();

            // проверки

            // есть ли пользователь в друзьях
            ResultSet isInFriendsAlreadyRS = statement.executeQuery(String.format("SELECT friends_id FROM users WHERE id = %d", currentUser.getId()));
            while (isInFriendsAlreadyRS.next()) {
                if (isInFriendsAlreadyRS.getString("friends_id").contains("-" + id + "-")) {
                    LOG.error("Ошибка при добавлении в друзья: пользователь уже есть в друзьях");
                    return 403; // 403 forbidden
                }
            }

            // существует ли уже такая заявка
            ResultSet requestRS = statement.executeQuery(String.format("SELECT id FROM friend_requests WHERE sender_id = %d AND getter_id = %d",
                    currentUser.getId(), id));

            if (requestRS.next()) {
                LOG.error("Ошибка при добавлении в друзья: запрос был отправлен ранее");
                return 403; // 403 forbidden
            }

            // отправка запроса если его нету
            if (!statement.executeQuery(String.format("SELECT * FROM friend_requests WHERE sender_id = %d AND getter_id = %d OR sender_id = %d AND getter_id = %d",
                    currentUser.getId(), id,
                    id, currentUser.getId())).next()) {

                statement.execute(String.format("INSERT INTO friend_requests (sender_id, getter_id) VALUES (%d, %d)", currentUser.getId(), id));
                LOG.info("Отправлена заявка в друзья: Отправитель -> " + currentUser.getId() + ", Получатель -> " + id);
                return 201; // 201 created
            }
            // принятние запроса если запрос существует

            // добавление в друзья пользователей
            int getter_id = currentUser.getId();
            int sender_id = id;

            String getter_friends = "";
            String sender_friends = "";

            ResultSet gfRS = statement.executeQuery(String.format("SELECT * FROM users WHERE id = %d", getter_id));
            while (gfRS.next()) {
                getter_friends = gfRS.getString("friends_id");
            }

            ResultSet sfRS = statement.executeQuery(String.format("SELECT * FROM users WHERE id = %d", sender_id));
            while (sfRS.next()) {
                sender_friends = sfRS.getString("friends_id");
            }

            statement.executeUpdate(String.format("UPDATE users SET friends_id = '%s' WHERE id = %d", (getter_friends + "-" + sender_id + "-"), getter_id));
            statement.executeUpdate(String.format("UPDATE users SET friends_id = '%s' WHERE id = %d", (sender_friends + "-" + getter_id + "-"), sender_id));

            // удаление заявки
            int idToDelete = 0;
            ResultSet getIdToDeleteRS = statement.executeQuery(String.format("SELECT id FROM friend_requests WHERE sender_id = %d AND getter_id = %d", sender_id, getter_id));
            while (getIdToDeleteRS.next()) {
                idToDelete = getIdToDeleteRS.getInt("id");
            }
            statement.execute(String.format("DELETE FROM friend_requests WHERE id = %d", idToDelete));

            LOG.info("Добавление в друзья: Отправитель -> " + sender_id + ", Получатель -> " + getter_id);
            return 201; // 201 created

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Ошибка при подключении к базе данных");
        }
    }

    /**
     * поиск пользователей по имяни/фамилии
     * @param search поисковой запрос
     * @return отфильтрованный список пользователей
     */
    public static List<User> search(String search) {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(Config.class);

        LOG.debug("search query: " + search);
        List<User> searchResult = new ArrayList<>();
        try {
            Connection connection = DBconnector.getConnection();
            Statement statement = connection.createStatement();

            ResultSet usersRS = statement.executeQuery("SELECT id,name FROM users");
            while (usersRS.next()) {
                User user = context.getBean("userBean",User.class);
                user.setId(usersRS.getInt("id"));
                user.setName(usersRS.getString("name"));
                searchResult.add(user);
            }
        } catch (SQLException e) {
            throw new RuntimeException("Ошибка при подключении к базе данных");
        }

        searchResult = searchResult.stream()
                .filter(s -> s.getName().toLowerCase().contains(search.toLowerCase()))
                .collect(Collectors.toList());
        context.close();
        return searchResult;
    }

    /**
     * изменить данные пользователя
     * @param name новое имя пользователя
     * @param status новый статус пользователя
     * @param filePart новая фотография пользователя
     */
    public static short edit(String name, String status, Part filePart,String userAgent) {
        // todo приватность

        /*
        закрытые личные сообщение (можно писать только если пользователь в списке друзей)
        приватный профиль (пользователи которых нету в списке друзей не увидят посты)
        скрытие списка друзей
         */
        LOG.debug("name: " + name);
        LOG.debug("status: " + status);
        LOG.debug("is photo updated: " + (filePart.getSize() != 0));
        try {
            Connection connection = DBconnector.getConnection();
            Statement statement = connection.createStatement();

            User currentUser = DataContainer.getUsersOnline().get(userAgent);
            statement.executeUpdate(String.format("UPDATE users SET status = '%s' WHERE id = %d",
                    status, currentUser.getId()));

            if (!name.equals(currentUser.getName())) {
                statement.execute(String.format("INSERT INTO name_change_requests (request_owner_id,old_name,new_name) VALUES (%d,'%s','%s')",
                        currentUser.getId(),currentUser.getName(),name));
                LOG.info("запрос на изменение имяни на " + name);
            }
            if (filePart != null && filePart.getSize() != 0) {
                uploadPhoto(filePart,userAgent);
            }
            currentUser.setStatus(status);
            return 202; // 202 accepted
        } catch (SQLException e) {
            throw new RuntimeException("Ошибка при подключении к базе данных");
        }
    }

    /**
     * загрузить фото пользователя в базу данных
     * @param filePart фото
     */
    private static short uploadPhoto(Part filePart,String userAgent) {
        try {
            InputStream inputStream = filePart.getInputStream();

            Connection connection = DBconnector.getConnection();
            String sql = "INSERT INTO profile_pictures (photo,owner_id) values (?,?)";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setBlob(1,inputStream);
            statement.setInt(2,DataContainer.getUsersOnline().get(userAgent).getId());

            int row = statement.executeUpdate();
            if (row > 0) {
                double fileSize = filePart.getSize();
                if (fileSize / 1024 < 1024) {
                    LOG.info("В базу данных успешно загружена фотография профиля. Размер файла: " + (int) (fileSize / 1024) + " KiB");
                } else {
                    LOG.info("В базу данных успешно загружена фотография профиля. Размер файла: " + String.format("%.2f",((fileSize / 1024) / 1024)) + " MB");
                }
                return 201; // 201 created
            } else {
                return 500; // 500 internal server error
            }
        } catch (SQLException e){
            e.printStackTrace();
            throw new RuntimeException("Ошибка при подключении к базе данных");
        } catch (IOException e) {}
        return 500; // 500 internal server error
    }

    /**
     * авто вход с запомненного браузера
     * @param userAgent информация о браузере
     */
    public static short autoLogin(String userAgent) {
        try {
            Connection connection = DBconnector.getConnection();
            Statement statement = connection.createStatement();

            ResultSet userAgentRs = statement.executeQuery(String.format("SELECT id, user_agent FROM users WHERE user_agent = '%s'",userAgent));

            if (userAgentRs.next()) {
                DataContainer.getUsersOnline().put(userAgent,UserDAO.getUserObject(userAgentRs.getInt("id")));

                Statement roleStatement = connection.createStatement();
                ResultSet roleRS = roleStatement.executeQuery("SELECT role FROM users WHERE user_agent = '" + userAgent + "'");
                roleRS.next();
                String role = roleRS.getString("role");
                DataContainer.getUsersOnline().get(userAgent).setUserRole(role.equals("USER") ? UserRoles.USER : UserRoles.ADMIN);

                LOG.info("Вход в аккаунт с устройства " + userAgent);

                return 200; // 200 ok
            }
        } catch (SQLException e) {
            throw new RuntimeException("Ошибка при подключении к базе данных");
        }
        return 404; // 404 not found
    }

    /**
     * выйти из аккаунта
     */
    public static short logout(String userAgent) {
        try {
            Connection connection = DBconnector.getConnection();
            Statement statement = connection.createStatement();
            User currentUser = DataContainer.getUsersOnline().get(userAgent);
            statement.executeUpdate("UPDATE users SET user_agent = 'none' WHERE id = " + currentUser.getId());
            LOG.info("Выход из аккаунта " + currentUser.getId());
        } catch (SQLException e) {
            throw new RuntimeException("Ошибка при подключении к базе данных");
        }
        return 200; // 200 ok
    }

    /**
     * принять заявку на смену имяни
     * @param id id запрашивающего нового имени
     * @param name новое имя
     */
    public static void acceptName(String id,String name) {
        try {
            Connection connection = DBconnector.getConnection();
            Statement statement = connection.createStatement();

            ResultSet requestOwnerRS = statement.executeQuery("SELECT * FROM name_change_requests WHERE id = " + id);
            requestOwnerRS.next();

            Statement userStatement = connection.createStatement();
            userStatement.executeUpdate(String.format("UPDATE users SET name = '%s' WHERE id = %d",name,requestOwnerRS.getInt("request_owner_id")));
            statement.close();

            Statement deleteStatement = connection.createStatement();
            deleteStatement.execute(String.format("DELETE FROM name_change_requests WHERE id = %d", Integer.parseInt(id)));

            LOG.info("Смена имени прошла успешно");
            LOG.debug(id + " " + name);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Ошибка при подключении к базе данных");
        }
    }

    /**
     * отклонить запрос на смену имени
     * @param id id запроса на смену имени
     */
    public static void rejectName(String id) {
        try {
            Connection connection = DBconnector.getConnection();
            Statement statement = connection.createStatement();

            statement.execute(String.format("DELETE FROM name_change_requests WHERE id = %d", Integer.parseInt(id)));

            LOG.info("заявка на смену имени отклонена");
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Ошибка при подключении к базе данных");
        }
    }

    /**
     * 5 друзей из списка друзей пользователя
     * @param id id пользователя чей список друзей надо найти
     * @return список друзей пользователя
     */
    public static List<User> getShortFriendList(int id) {
        List<User> friends = new ArrayList<>();
        try {
            Connection connection = DBconnector.getConnection();
            Statement statement = connection.createStatement();

            ResultSet frindsRS = statement.executeQuery("SELECT * FROM users WHERE friends_id LIKE '%-" + id + "-%' LIMIT 5");

            while (frindsRS.next()) {
                int friend_id = frindsRS.getInt("id");
                friends.add(getUserObject(friend_id));
                LOG.debug("" + friend_id);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Ошибка при подключении к базе данных");
        }
        return friends;
    }
}