package WayKey.Configuration;

import WayKey.Entities.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class Config {
    @Bean
    @Scope("prototype")
    public User userBean() {
        return new User();
    }

    @Bean
    @Scope("prototype")
    public Comment commentBean() {
        return new Comment();
    }

    @Bean
    @Scope("prototype")
    public Post postBean() {
        return new Post();
    }

    @Bean
    @Scope("prototype")
    public Message messageBean() {
        return new Message();
    }

    @Bean
    @Scope("prototype")
    public NameRequest nameRequestBean() {
        return new NameRequest();
    }

    @Bean
    @Scope("prototype")
    public TechSupportRequest techSupportRequestBean() {
        return new TechSupportRequest();
    }
}
