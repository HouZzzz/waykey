function sendRequest(method, url,body = null) {
    return new Promise((resolve,reject) => {
        const xhr = new XMLHttpRequest()

        xhr.open(method,url)

        xhr.responseType = 'json'
        xhr.setRequestHeader('Content-Type','application/json')

        xhr.onload = () => {
            if (xhr.status >= 400) {
                reject(xhr.response)
            } else {
                resolve(xhr.response)
            }
        }

        xhr.onerror = () => {
            reject(xhr.response)
        }

        xhr.send(JSON.stringify(body))
    })
}

var frm = document.getElementsByName("post");

for(var i=0;i<frm.length;i++) {
    let currFrm = frm[i];
    currFrm.like.onchange = function() {
        //console.log(currFrm.like.checked);
        const body = {
            status: currFrm.like.checked,
            id: currFrm.id.value
        }
        if (currFrm.like.checked == false) {
            currFrm.getElementsByTagName("b")[0].innerHTML -= 1;
        } else {
            currFrm.getElementsByTagName("b")[0].innerHTML++;
        }
        sendRequest('POST','http://localhost:8080/like',body)
    }
    currFrm.sendComment.onclick = function() {
        let message = currFrm.comment.value
        if (message.isEmpty) {
        } else {
            const body = {
                text: message,
                id: currFrm.id.value
            }
            sendRequest('POST','http://localhost:8080/comment',body)
            currFrm.comment.value = ""
            if ((window.location.href).includes("/post/")) {
                document.location.reload();
            }
        }
    }
}

function selectChecker() {
    var selectedFilter = document.getElementById("select").value;

    if (selectedFilter == 'newest') {
        window.location.replace("http://localhost:8080/news?filter=newest");
    } else if (selectedFilter == 'most_popular') {
        window.location.replace("http://localhost:8080/news?filter=most_popular");
    }
}
// document.location.reload(); обновление страницы